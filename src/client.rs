use crate::{
    message::{Command, CommitEntry},
    NodeId,
};
use std::sync::{Arc, Mutex};
use tokio::sync::mpsc;

pub struct Client {
    pub id: NodeId,
    pub received_commands: Arc<Mutex<Vec<Command>>>,
}

impl Client {
    pub fn commands(&self) -> Vec<Command> {
        let cmds = self.received_commands.lock().unwrap();
        (*cmds).clone()
    }

    pub fn has_command(&self, cmd: &Command) -> bool {
        let cmds = self.received_commands.lock().unwrap();
        (*cmds).contains(cmd)
    }

    pub fn clear_commands(&self) {
        let mut cmds = self.received_commands.lock().unwrap();
        (*cmds).clear();
    }
}

pub async fn start_receiving_commit_ch(
    mut commit_rx: mpsc::UnboundedReceiver<CommitEntry>,
    received_commands: Arc<Mutex<Vec<Command>>>,
    client_id: NodeId,
) -> tokio::task::JoinHandle<()> {
    tokio::spawn(async move {
        loop {
            match commit_rx.recv().await {
                Some(msg) if msg.command == Command::Stop => break,
                Some(msg) => {
                    tracing::info!("Client {} got command: {:?}", client_id, msg.command);
                    let mut cmds = received_commands.lock().unwrap();
                    (*cmds).push(msg.command);
                }
                None => {
                    tracing::info!("Commit Channel {client_id} is Closed");
                    break;
                }
            }
        }
    })
}
