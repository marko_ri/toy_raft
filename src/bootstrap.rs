use crate::{message::Command, raft::CmState, GenericError, NodeId, Term};

use std::collections::HashMap;
use std::str::FromStr;
use tokio::sync::mpsc;

pub fn read_config_file(file_name: &str) -> Vec<NodeCfg> {
    let mut result = Vec::new();
    let path = std::path::Path::new(file_name);
    let conf = tini::Ini::from_file(path).expect("config file");

    for (section, _kvs) in conf.iter() {
        let id = conf.get(section, "id").expect("id");
        let listen = conf.get(section, "listen").expect("listen");
        let peers: String = conf.get(section, "peers").expect("peers");
        let peers = PeersCfg::from_str(&peers).expect("parsed peers map").0;
        let client = conf.get(section, "client").expect("client");
        let node_cfg = NodeCfg {
            id,
            listen,
            peers,
            client,
        };
        result.push(node_cfg);
    }

    result
}

pub fn tracing_config(file_name: &str) -> tracing_appender::non_blocking::WorkerGuard {
    let file_appender = tracing_appender::rolling::daily("./logs", file_name);
    let (non_blocking, guard) = tracing_appender::non_blocking(file_appender);
    tracing_subscriber::fmt()
        .with_writer(non_blocking)
        .with_max_level(tracing::Level::DEBUG)
        .with_target(false)
        .with_ansi(false)
        .init();
    guard
}

pub struct ServerTestingChannels {
    pub play_dead_rx: mpsc::Receiver<bool>,
    pub terminate_rx: mpsc::Receiver<()>,
    pub report_req_rx: mpsc::Receiver<()>,
    pub report_res_tx: mpsc::Sender<(NodeId, Term, CmState)>,
    pub submit_req_rx: mpsc::Receiver<Command>,
    pub submit_res_tx: mpsc::Sender<bool>,
}

#[derive(Debug, Clone)]
struct PeersCfg(HashMap<NodeId, String>);

impl FromStr for PeersCfg {
    type Err = GenericError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut result = HashMap::new();

        for s in input.split('|') {
            let (id, addr) = s
                .trim()
                .strip_prefix('(')
                .and_then(|s| s.strip_suffix(')'))
                .and_then(|s| s.split_once(','))
                .ok_or::<Self::Err>("can not parse peer tuple".into())?;
            let id = id.trim().parse::<NodeId>()?;
            let addr = addr.trim().to_string();
            result.insert(id, addr);
        }

        Ok(PeersCfg(result))
    }
}

#[derive(Debug, Clone)]
pub struct NodeCfg {
    pub id: NodeId,
    pub listen: String,
    pub peers: HashMap<NodeId, String>,
    pub client: NodeId,
}
