pub mod bootstrap;
pub mod client;
pub mod message;
pub mod raft;
pub mod server;

pub type GenericError = Box<dyn std::error::Error + Send + Sync + 'static>;
pub type GenericResult<T> = Result<T, GenericError>;
pub type NodeId = i32;
pub type Term = i32;
pub type LogIndex = i32;
