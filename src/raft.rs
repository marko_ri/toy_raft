// Server is running the event loop. All async calls are there. Here we should
// have only regular non blocking functions. UnboundedSender is used to
// comunicate with the server (which is not marked async)

use crate::{message::*, GenericResult, LogIndex, NodeId, Term};
use std::{
    collections::{HashMap, HashSet},
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};
use tokio::sync::mpsc;

// send Heartbeat every 5th TICK_INTERVAL
const HEARTBEAT_INTERVAL: u16 = 5;

#[derive(Default, Debug, PartialEq, Clone)]
pub enum CmState {
    #[default]
    Follower,
    Candidate,
    Leader,
    Dead,
}

// ConsensusModule (CM) implements a single node of Raft consensus.
#[derive(Debug)]
pub struct ConsensusModule {
    pub id: NodeId,
    peer_ids: HashSet<NodeId>,
    storage: Arc<Mutex<dyn Storage>>,
    current_term: Term,
    voted_for: NodeId,
    log: Vec<LogEntry>,
    state: CmState,
    election_reset_event: Instant,
    node_tx: mpsc::UnboundedSender<Message>,
    election_timeout: Duration,
    votes_received: u16,
    heartbeat_count: u16,
    election_start_term: Term,
    heartbeat_start_term: Term,
    commit_index: LogIndex,
    last_applied: LogIndex,
    next_index: HashMap<NodeId, LogIndex>,
    match_index: HashMap<NodeId, LogIndex>,
}

impl ConsensusModule {
    pub fn new(
        id: NodeId,
        peer_ids: HashSet<NodeId>,
        node_tx: mpsc::UnboundedSender<Message>,
        storage: Arc<Mutex<dyn Storage>>,
    ) -> Self {
        let mut s = Self {
            id,
            peer_ids,
            storage,
            current_term: 0,
            voted_for: -1,
            log: Vec::new(),
            state: CmState::Follower,
            election_reset_event: Instant::now(),
            node_tx,
            election_timeout: rand_duration(),
            votes_received: 0,
            heartbeat_count: 0,
            election_start_term: 0,
            heartbeat_start_term: 0,
            commit_index: -1,
            last_applied: -1,
            next_index: HashMap::new(),
            match_index: HashMap::new(),
        };
        let has_data = {
            let storage = s.storage.lock().unwrap();
            storage.has_data()
        };
        if has_data {
            s.restore_from_storage().expect("restore_from_storage");
        }

        s
    }

    fn restore_from_storage(&mut self) -> GenericResult<()> {
        // TODO: copy then deserialize (so we do not hold lock)?
        let storage = self.storage.lock().expect("mutex lock");

        match (*storage).get("current_term") {
            Some(bytes) => {
                tracing::debug!("Restoring current_term from storage");
                let decoded = bincode::deserialize(bytes)?;
                self.current_term = decoded;
            }
            None => tracing::debug!("current_term not found in storage"),
        }
        match (*storage).get("voted_for") {
            Some(bytes) => {
                tracing::debug!("Restoring voted_for from storage");
                let decoded = bincode::deserialize(bytes)?;
                self.voted_for = decoded;
            }
            None => tracing::debug!("voted_for not found in storage"),
        }
        match (*storage).get("log") {
            Some(bytes) => {
                tracing::debug!("Restoring log from storage");
                let decoded = bincode::deserialize(bytes)?;
                self.log = decoded;
            }
            None => tracing::debug!("log not found in storage"),
        }

        Ok(())
    }

    fn persist_to_storage(&mut self) -> GenericResult<()> {
        let current_term = bincode::serialize(&self.current_term)?;
        let voted_for = bincode::serialize(&self.voted_for)?;
        let log = bincode::serialize(&self.log)?;

        let mut storage = self.storage.lock().expect("mutex lock");
        (*storage).set("current_term", current_term);
        (*storage).set("voted_for", voted_for);
        (*storage).set("log", log);

        Ok(())
    }

    pub fn report(&self) -> (NodeId, Term, CmState) {
        (self.id, self.current_term, self.state.clone())
    }

    #[tracing::instrument(skip_all, fields(self.id = self.id))]
    pub fn play_dead(&mut self, flag: bool) {
        if flag {
            self.state = CmState::Dead;
            tracing::info!(message = "Becomes Dead");
        } else {
            tracing::info!(message = "Becomes Undead");
        }
    }

    #[tracing::instrument(skip_all, fields(self.id = self.id, candidate_id = args.candidate_id))]
    fn request_vote(&mut self, args: RequestVoteArgs) -> GenericResult<()> {
        if self.state == CmState::Dead {
            return Ok(());
        }
        let (last_log_index, last_log_term) = self.last_log_index_and_term();

        if args.term > self.current_term {
            tracing::info!("term out of date");
            self.become_follower(args.term);
        }

        let vote_granted = if self.current_term == args.term
            && (self.voted_for == -1 || self.voted_for == args.candidate_id)
            && (args.last_log_term > last_log_term
                || (args.last_log_term == last_log_term && args.last_log_index >= last_log_index))
        {
            self.voted_for = args.candidate_id;
            self.election_reset_event = Instant::now();
            true
        } else {
            false
        };
        let reply = RequestVoteReply {
            term: self.current_term,
            vote_granted,
        };
        tracing::info!(reply = ?reply);
        self.persist_to_storage()?;

        self.send(
            ToAddr::Node(args.candidate_id),
            Payload::RequestVoteReply(reply),
        )?;
        Ok(())
    }

    #[tracing::instrument(skip(self), fields(self.id = self.id))]
    fn append_entries(&mut self, mut args: AppendEntriesArgs) -> GenericResult<()> {
        if self.state == CmState::Dead {
            return Ok(());
        }

        if args.term > self.current_term {
            tracing::info!("term out of date");
            self.become_follower(args.term);
        }

        let mut success = false;
        let mut conflict_index: LogIndex = -1;
        let mut conflict_term: LogIndex = -1;
        let saved_entries_len = args.entries.len() as LogIndex;
        if args.term == self.current_term {
            if self.state != CmState::Follower {
                self.become_follower(args.term);
            }
            self.election_reset_event = Instant::now();

            if args.prev_log_index == -1
                || (args.prev_log_index < self.log.len() as LogIndex
                    && args.prev_log_term == self.log[args.prev_log_index as usize].term)
            {
                success = true;
                // Find an insertion point - where there's a term mismatch
                // between the existing log starting at PrevLogIndex+1 and
                // the new entries sent in the RPC
                let mut log_insert_index = args.prev_log_index + 1;
                let mut new_entries_index = 0;
                loop {
                    if log_insert_index >= self.log.len() as LogIndex
                        || new_entries_index >= args.entries.len()
                    {
                        break;
                    }
                    if self.log[log_insert_index as usize].term
                        != args.entries[new_entries_index].term
                    {
                        break;
                    }
                    log_insert_index += 1;
                    new_entries_index += 1;
                }
                // At the end of this loop:
                // - logInsertIndex points at the end of the log, or an index
                // where the term mismatches with an entry from the leader
                // - newEntriesIndex points at the end of Entries, or an index
                // where the term mismatches with the corresponding log entry
                if new_entries_index < args.entries.len() {
                    self.log.truncate(log_insert_index as usize);
                    let mut args_entries = args.entries.drain(new_entries_index..).collect();
                    self.log.append(&mut args_entries);
                    tracing::info!(message = "Entry inserted. Log is now", self.log = ?self.log);
                }

                if args.leader_commit > self.commit_index {
                    self.commit_index = args.leader_commit.min(self.log.len() as LogIndex - 1);
                    tracing::info!("setting commmit_index to {}", self.commit_index);
                    self.send_to_commit_chan()?;
                }
            } else {
                // No match for PrevLogIndex/PrevLogTerm. Populate
                // ConflictIndex/ConflictTerm to help the leader bring us
                // up to date quickly
                if args.prev_log_index >= self.log.len() as LogIndex {
                    conflict_index = self.log.len() as LogIndex;
                    conflict_term = -1;
                } else {
                    // PrevLogIndex points within our log, but PrevLogTerm
                    // doesn't match self.log[prev_log_index]
                    conflict_term = self.log[args.prev_log_index as usize].term;
                    let mut j = -1;
                    for i in (0..args.prev_log_index).rev() {
                        if self.log[i as usize].term != conflict_term {
                            j = i;
                            break;
                        }
                    }
                    conflict_index = j + 1;
                }
            }
        }

        let reply = AppendEntriesReply {
            term: self.current_term,
            success,
            from_node: self.id,
            next_index: args.next_index,
            entries_len: saved_entries_len,
            conflict_index,
            conflict_term,
        };
        tracing::info!(reply = ?reply);
        self.persist_to_storage()?;

        self.send(
            ToAddr::Node(args.leader_id),
            Payload::AppendEntriesReply(reply),
        )?;
        Ok(())
    }

    pub fn tick(&mut self) -> GenericResult<()> {
        if self.state == CmState::Leader {
            if self.heartbeat_count > HEARTBEAT_INTERVAL {
                self.leader_send_heartbeats()?;
                self.heartbeat_count = 0;
            }
            self.heartbeat_count += 1;
        } else if self.election_reset_event.elapsed() > self.election_timeout {
            // Start an election if we haven't heard from a leader or
            // haven't voted for someone for the duration of the timeout
            self.start_election()?;
        }

        Ok(())
    }

    #[tracing::instrument(skip_all, fields(self.id = self.id, msg = %msg.payload))]
    pub fn handle_tcp_reply(&mut self, msg: Message) -> GenericResult<()> {
        match msg.payload {
            Payload::RequestVoteArgs(args) => {
                self.request_vote(args)?;
            }
            Payload::RequestVoteReply(reply) => {
                tracing::info!(reply = ?reply);
                if self.state != CmState::Candidate {
                    tracing::info!(message = "while waiting for reply, state changed:", state = ?self.state);
                    return Ok(());
                }
                match reply.term.cmp(&self.election_start_term) {
                    std::cmp::Ordering::Greater => {
                        tracing::info!("term out of date");
                        self.become_follower(reply.term);
                    }
                    std::cmp::Ordering::Equal => {
                        if reply.vote_granted {
                            self.votes_received += 1;
                            if self.votes_received * 2 > self.peer_ids.len() as u16 + 1 {
                                tracing::info!("wins election with {} votes", self.votes_received);
                                // next tick will call leader_send_heartbeat
                                self.state = CmState::Leader;
                                for peer_id in self.peer_ids.iter() {
                                    self.next_index.insert(*peer_id, self.log.len() as LogIndex);
                                    self.match_index.insert(*peer_id, -1);
                                }
                            }
                        }
                    }
                    std::cmp::Ordering::Less => {}
                }
            }
            Payload::AppendEntriesArgs(args) => {
                self.append_entries(args)?;
            }
            Payload::AppendEntriesReply(reply) => {
                if self.state != CmState::Leader {
                    return Ok(());
                }

                if reply.term > self.current_term {
                    tracing::info!("term out of date");
                    self.become_follower(reply.term);
                    return Ok(());
                }
                if self.state == CmState::Leader && self.heartbeat_start_term == reply.term {
                    let peer_id = reply.from_node;
                    if reply.success {
                        self.next_index
                            .insert(peer_id, reply.next_index + reply.entries_len);
                        self.match_index
                            .insert(peer_id, self.next_index[&peer_id] - 1);
                        tracing::info!(
                            message = "AppendEntriesReply success", from_node = peer_id, ?self.next_index, ?self.match_index);
                        let saved_commit_index = self.commit_index;
                        let log_len = self.log.len() as LogIndex;
                        for i in self.commit_index + 1..log_len {
                            if self.log[i as usize].term == self.current_term {
                                let mut match_count = 1;
                                for peer_id in self.peer_ids.iter() {
                                    if self.match_index[peer_id] >= i {
                                        match_count += 1;
                                    }
                                }
                                if match_count * 2 > self.peer_ids.len() + 1 {
                                    self.commit_index = i;
                                }
                            }
                        }
                        if self.commit_index != saved_commit_index {
                            tracing::info!("leader sets commit_index: {}", self.commit_index);
                            self.send_to_commit_chan()?;
                            self.leader_send_heartbeats()?;
                        }
                    } else {
                        if reply.conflict_term >= 0 {
                            let mut last_index_of_term: LogIndex = -1;
                            for i in (0..self.log.len()).rev() {
                                if self.log[i].term == reply.conflict_term {
                                    last_index_of_term = i as LogIndex;
                                    break;
                                }
                            }
                            if last_index_of_term >= 0 {
                                self.next_index.insert(peer_id, last_index_of_term + 1);
                            } else {
                                self.next_index.insert(peer_id, reply.conflict_index);
                            }
                        } else {
                            self.next_index.insert(peer_id, reply.conflict_index);
                        }
                        tracing::info!(
                            message = "AppendEntriesReply !success from",
                            peer_id = peer_id,
                            next_index = reply.next_index - 1,
                        );
                    }
                }
            }
            _ => unreachable!(),
        }

        Ok(())
    }

    #[tracing::instrument(skip(self), fields(self.id = self.id))]
    fn start_election(&mut self) -> GenericResult<()> {
        self.state = CmState::Candidate;
        self.current_term += 1;
        self.election_start_term = self.current_term;
        self.election_reset_event = Instant::now();
        self.election_timeout = rand_duration();
        self.voted_for = self.id;
        tracing::info!(
            message = "becomes candidate",
            current_term = self.current_term,
        );
        self.votes_received = 1;
        let (saved_last_log_index, saved_last_log_term) = self.last_log_index_and_term();

        let req = RequestVoteArgs {
            term: self.election_start_term,
            candidate_id: self.id,
            last_log_index: saved_last_log_index,
            last_log_term: saved_last_log_term,
        };
        self.send(ToAddr::Broadcast, Payload::RequestVoteArgs(req))?;

        Ok(())
    }

    #[tracing::instrument(skip(self), fields(self.id = self.id))]
    fn become_follower(&mut self, term: Term) {
        self.state = CmState::Follower;
        self.current_term = term;
        self.voted_for = -1;
        self.election_reset_event = Instant::now();
        self.election_timeout = rand_duration();
    }

    #[tracing::instrument(skip(self), fields(self.id = self.id))]
    fn leader_send_heartbeats(&mut self) -> GenericResult<()> {
        self.heartbeat_start_term = self.current_term;
        for peer_id in self.peer_ids.iter() {
            let ni = self.next_index[&peer_id];
            let prev_log_index = ni - 1;
            let prev_log_term = if prev_log_index >= 0 {
                self.log[prev_log_index as usize].term
            } else {
                -1
            };
            let entries = self.log[ni as usize..].to_vec();
            let req = AppendEntriesArgs {
                term: self.heartbeat_start_term,
                leader_id: self.id,
                prev_log_index,
                prev_log_term,
                entries,
                leader_commit: self.commit_index,
                next_index: ni,
            };
            tracing::info!(message = "sending AppendEntries to", peer_id, ni);
            self.send(ToAddr::Node(*peer_id), Payload::AppendEntriesArgs(req))?;
        }
        Ok(())
    }

    // It returns true iff this CM is the leader - in which case the command
    // is accepted. If false is returned, the client will have to find
    // a different CM to submit this command to.
    #[tracing::instrument(skip(self), fields(self.id = self.id, self.state = ?self.state))]
    pub fn submit(&mut self, command: Command) -> GenericResult<bool> {
        if self.state == CmState::Leader {
            self.log.push(LogEntry {
                command,
                term: self.current_term,
            });
            self.persist_to_storage()?;
            tracing::info!(self.log = ?self.log);
            self.leader_send_heartbeats()?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    // In the article this is go routine that listens on one extra channel.
    // The reason is that this is potentially blocking call: when node comes
    // online again, there could be lots of messages that need to go to client
    // (our buffered channels should be ok for now.... well, unbounded buffers :))
    #[tracing::instrument(skip(self), fields(self.id = self.id))]
    fn send_to_commit_chan(&mut self) -> GenericResult<()> {
        if self.commit_index <= self.last_applied {
            return Ok(());
        }
        let saved_term = self.current_term;
        let saved_last_applied = self.last_applied;
        let entries =
            self.log[(self.last_applied + 1) as usize..(self.commit_index + 1) as usize].to_vec();
        self.last_applied = self.commit_index;
        tracing::info!(entries = ?entries, saved_last_applied);
        for (i, entry) in entries.into_iter().enumerate() {
            let ce = CommitEntry {
                command: entry.command,
                index: saved_last_applied + i as LogIndex + 1,
                term: saved_term,
            };
            self.send(ToAddr::Client, Payload::CommitEntry(ce))?;
        }
        Ok(())
    }

    fn last_log_index_and_term(&self) -> (LogIndex, Term) {
        if !self.log.is_empty() {
            let last_index = self.log.len() - 1;
            (last_index as LogIndex, self.log[last_index].term)
        } else {
            (-1, -1)
        }
    }

    fn send(&self, to: ToAddr, payload: Payload) -> GenericResult<()> {
        let msg = Message {
            term: self.current_term,
            from: self.id,
            to,
            payload,
        };
        //tracing::debug!("Sending {msg:?}");
        Ok(self.node_tx.send(msg)?)
    }
}

fn rand_duration() -> Duration {
    let ms = fastrand::u64(150..300);
    Duration::from_millis(ms)
}
