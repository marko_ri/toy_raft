// https://github.com/erikgrinaker/toydb/blob/master/src/raft/server.rs

use crate::{
    bootstrap::{NodeCfg, ServerTestingChannels},
    message::{CommitEntry, Message, Payload, Storage, ToAddr},
    raft::ConsensusModule,
    GenericResult, NodeId,
};

use futures::SinkExt;
use std::collections::HashMap;
use std::sync::{Arc, Mutex, OnceLock};
use std::time::Duration;
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc;
use tokio_stream::wrappers::{ReceiverStream, TcpListenerStream, UnboundedReceiverStream};
use tokio_stream::StreamExt as _;
use tokio_util::codec::{Framed, LengthDelimitedCodec};

// Interval between Raft ticks
const TICK_INTERVAL: Duration = Duration::from_millis(10);
pub static NODES: OnceLock<Vec<NodeCfg>> = OnceLock::new();

pub struct Server {
    node: ConsensusModule,
    peers: HashMap<NodeId, String>,
    node_rx: mpsc::UnboundedReceiver<Message>,
}

impl Server {
    pub fn new(
        id: NodeId,
        peers: HashMap<NodeId, String>,
        storage: Arc<Mutex<dyn Storage>>,
    ) -> Self {
        let (node_tx, node_rx) = mpsc::unbounded_channel::<Message>();
        let node = ConsensusModule::new(id, peers.keys().copied().collect(), node_tx, storage);
        Self {
            node,
            peers,
            node_rx,
        }
    }

    // Connects to peers and serves requests
    pub async fn serve(
        self,
        addr: String,
        testing_channels: ServerTestingChannels,
        commit_tx: mpsc::UnboundedSender<CommitEntry>,
        reconnected: bool,
    ) {
        let listener = TcpListener::bind(addr).await.expect("tcp bind");
        let (tcp_in_tx, tcp_in_rx) = mpsc::unbounded_channel::<Message>();
        let (tcp_out_tx, tcp_out_rx) = mpsc::unbounded_channel::<Message>();
        let self_id = self.node.id;

        // peer_txs map is needed, because we'll allow nodes to disconnect/reconnect
        let mut peer_txs: HashMap<NodeId, mpsc::Sender<Message>> = HashMap::new();
        for (id, addr) in self.peers.iter() {
            let tx = Self::create_connection_to_peer(addr, self_id).await;
            peer_txs.insert(*id, tx);
        }
        let peer_txs = Arc::new(Mutex::new(peer_txs));

        // we need to send this message on reconnect, so that "to" peer
        // gets info about "from" peer (so that it can connect back)
        if reconnected {
            let msg = Message {
                term: -1,
                from: self_id,
                to: ToAddr::Broadcast,
                payload: Payload::Reconnected,
            };
            tcp_out_tx.send(msg).expect("send reconnected msg");
        }

        let result = tokio::try_join!(
            Self::tcp_receive(listener, tcp_in_tx, self_id, peer_txs.clone()),
            Self::tcp_send(tcp_out_rx, peer_txs),
            Self::eventloop(
                self.node,
                self.node_rx,
                tcp_in_rx,
                tcp_out_tx,
                testing_channels,
                commit_tx,
            )
        );

        if let Err(e) = result {
            tracing::error!("Serve Error: {e:?}");
            //panic!("{e}");
        }
    }

    // Runs the event loop with support for testing
    // (few extra local channels that facilitate communication with nodes)
    async fn eventloop(
        mut node: ConsensusModule,
        node_rx: mpsc::UnboundedReceiver<Message>,
        tcp_rx: mpsc::UnboundedReceiver<Message>,
        tcp_tx: mpsc::UnboundedSender<Message>,
        mut tch: ServerTestingChannels,
        commit_tx: mpsc::UnboundedSender<CommitEntry>,
    ) -> GenericResult<()> {
        let mut node_rx = UnboundedReceiverStream::new(node_rx);
        let mut tcp_rx = UnboundedReceiverStream::new(tcp_rx);
        let mut ticker = tokio::time::interval(TICK_INTERVAL);

        let mut play_dead = false;
        loop {
            tokio::select! {
                Some(flag) = tch.play_dead_rx.recv() => {
                    node.play_dead(flag);
                    play_dead = flag;
                },
                Some(()) = tch.terminate_rx.recv() => {
                    tracing::info!("TERMINATED NODE {}", node.id);
                    return Err(format!("Terminated node {}", node.id).into());
                },
                Some(()) = tch.report_req_rx.recv() => {
                    let res = node.report();
                    tch.report_res_tx.send(res).await?;
                },
                Some(cmd) = tch.submit_req_rx.recv() => {
                    let res = node.submit(cmd)?;
                    tch.submit_res_tx.send(res).await?;
                },
                _ = ticker.tick() => node.tick()?,
                Some(msg) = tcp_rx.next(), if !play_dead => node.handle_tcp_reply(msg)?,
                Some(msg) = node_rx.next(), if !play_dead => {
                    match msg.payload {
                        Payload::CommitEntry(entry) => {
                            commit_tx.send(entry)?;
                        },
                        _ => {
                            tcp_tx.send(msg)?;
                        },
                    }
                },
            }
        }
    }

    // Receives inbound messages from peers via TCP
    #[tracing::instrument(skip_all, fields(self.id = self_id))]
    async fn tcp_receive(
        listener: TcpListener,
        in_tx: mpsc::UnboundedSender<Message>,
        self_id: NodeId,
        peer_txs: Arc<Mutex<HashMap<NodeId, mpsc::Sender<Message>>>>,
    ) -> GenericResult<()> {
        let mut listener = TcpListenerStream::new(listener);
        while let Some(socket) = listener.try_next().await? {
            let peer_txs = Arc::clone(&peer_txs);
            let peer = socket.peer_addr()?;
            tracing::debug!(message = "Accepted connection from", ?peer);
            let peer_in_tx = in_tx.clone();
            tokio::spawn(async move {
                match Self::tcp_receive_peer(socket, peer_in_tx, self_id, peer_txs).await {
                    Ok(()) => tracing::debug!("Node {self_id} received disconnect from {peer:?}"),
                    Err(e) => tracing::error!("Node {self_id} received error {e}"),
                };
            });
        }

        Ok(())
    }

    // Receives inbound messages from a peer via TCP
    async fn tcp_receive_peer(
        socket: TcpStream,
        in_tx: mpsc::UnboundedSender<Message>,
        self_id: NodeId,
        peer_txs: Arc<Mutex<HashMap<NodeId, mpsc::Sender<Message>>>>,
    ) -> GenericResult<()> {
        let mut stream = tokio_serde::SymmetricallyFramed::<_, Message, _>::new(
            Framed::new(socket, LengthDelimitedCodec::new()),
            tokio_serde::formats::SymmetricalBincode::<Message>::default(),
        );
        while let Some(msg) = stream.try_next().await? {
            match msg.payload {
                Payload::Reconnected => {
                    tracing::info!(
                        "Node {self_id} received RECONNECTED message from node {}",
                        msg.from
                    );
                    let node_cfg = NODES
                        .get()
                        .unwrap()
                        .iter()
                        .find(|&n| n.id == self_id)
                        .unwrap();
                    let addr = node_cfg.peers.get(&msg.from).unwrap();
                    let peer_tx = Self::create_connection_to_peer(addr, self_id).await;
                    let Ok(mut peer_txs_mg) = peer_txs.lock() else {
                        return Err("can not obtain lock".into());
                    };
                    (*peer_txs_mg).insert(msg.from, peer_tx);
                }
                _ => in_tx.send(msg)?,
            }
        }
        Ok(())
    }

    // Sends outbound messages to peers via TCP
    async fn tcp_send(
        out_rx: mpsc::UnboundedReceiver<Message>,
        peer_txs: Arc<Mutex<HashMap<NodeId, mpsc::Sender<Message>>>>,
    ) -> GenericResult<()> {
        let mut out_rx = UnboundedReceiverStream::new(out_rx);

        while let Some(message) = out_rx.next().await {
            let to = match message.to {
                ToAddr::Broadcast => {
                    let Ok(mg) = peer_txs.lock() else {
                        return Err("can not obtain lock".into());
                    };
                    (*mg).keys().copied().collect()
                }
                ToAddr::Node(peer) => vec![peer],
                _ => unreachable!(),
            };
            for id in to {
                let Ok(mut mg) = peer_txs.lock() else {
                    return Err("can not obtain lock".into());
                };
                if let Some(tx) = (*mg).get_mut(&id) {
                    match tx.try_send(message.clone()) {
                        Ok(()) => {}
                        Err(mpsc::error::TrySendError::Full(_)) => {
                            tracing::debug!("Full send buffer for peer {id}, discarding message");
                        }
                        Err(e) => return Err(e.into()),
                    }
                }
            }
        }
        Ok(())
    }

    // Sends outbound messages to a peer. Retrying until message is successfully sent
    #[tracing::instrument(skip_all, fields(self.id = self_id))]
    async fn tcp_send_peer(addr: String, out_rx: mpsc::Receiver<Message>, self_id: NodeId) {
        let mut out_rx = ReceiverStream::new(out_rx);
        loop {
            match TcpStream::connect(&addr).await {
                Ok(socket) => {
                    tracing::debug!(message = "Connected", addr);
                    match Self::tcp_send_peer_session(socket, &mut out_rx).await {
                        Ok(()) => break,
                        Err(e) => tracing::error!("Failed sending to {addr}: {e}"),
                    }
                }
                Err(e) => tracing::error!("Failed connecting to {addr}: {e}"),
            }
            tokio::time::sleep(Duration::from_millis(1000)).await;
        }
        tracing::debug!(message = "Disconnected", addr);
    }

    // Sends outbound messages to a peer via TCP
    async fn tcp_send_peer_session(
        socket: TcpStream,
        out_rx: &mut ReceiverStream<Message>,
    ) -> GenericResult<()> {
        let mut stream = tokio_serde::SymmetricallyFramed::<_, Message, _>::new(
            Framed::new(socket, LengthDelimitedCodec::new()),
            tokio_serde::formats::SymmetricalBincode::<Message>::default(),
        );
        while let Some(message) = out_rx.next().await {
            stream.send(message).await?;
        }
        Ok(())
    }

    async fn create_connection_to_peer<T: ToString>(
        addr: T,
        self_id: NodeId,
    ) -> mpsc::Sender<Message> {
        let (tx, rx) = mpsc::channel::<Message>(1000);
        tokio::spawn(Self::tcp_send_peer(addr.to_string(), rx, self_id));
        tx
    }
}
