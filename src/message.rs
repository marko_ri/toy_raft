use crate::{LogIndex, NodeId, Term};
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ToAddr {
    Broadcast,
    Node(NodeId),
    Client,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Message {
    // the current term of the sender
    pub term: Term,
    pub from: NodeId,
    pub to: ToAddr,
    pub payload: Payload,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Payload {
    RequestVoteArgs(RequestVoteArgs),
    RequestVoteReply(RequestVoteReply),
    AppendEntriesArgs(AppendEntriesArgs),
    AppendEntriesReply(AppendEntriesReply),
    CommitEntry(CommitEntry),
    Reconnected,
}

impl fmt::Display for Payload {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let result = match self {
            Payload::RequestVoteArgs(_) => "RequestVoteArgs",
            Payload::RequestVoteReply(_) => "RequestVoteReply",
            Payload::AppendEntriesArgs(_) => "AppendEntriesArgs",
            Payload::AppendEntriesReply(_) => "AppendEntriesReply",
            Payload::CommitEntry(_) => "CommitEntry",
            Payload::Reconnected => "Reconnected",
        };
        write!(f, "{result}")
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RequestVoteArgs {
    pub term: Term,
    pub candidate_id: NodeId,
    pub last_log_index: LogIndex,
    pub last_log_term: Term,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RequestVoteReply {
    pub term: Term,
    pub vote_granted: bool,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AppendEntriesArgs {
    pub term: Term,
    pub leader_id: NodeId,
    pub prev_log_index: LogIndex,
    pub prev_log_term: Term,
    pub entries: Vec<LogEntry>,
    pub leader_commit: LogIndex,
    pub next_index: LogIndex,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AppendEntriesReply {
    pub term: Term,
    pub success: bool,
    pub from_node: NodeId,
    pub next_index: LogIndex,
    pub entries_len: LogIndex,
    pub conflict_index: LogIndex,
    pub conflict_term: Term,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Command {
    Get,
    Add(i32),
    Delete,
    Stop,
}

// CommitEntry is the data reported by Raft to the commit channel. Each commit
// entry notifies the client that consensus was reached on a command and
// it can be applied to the client's state machine.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CommitEntry {
    // Command is the client command being committed.
    pub command: Command,
    // Index is the log index at which the client command is committed.
    pub index: LogIndex,
    // Term is the Raft term at which the client command is committed.
    pub term: Term,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct LogEntry {
    pub command: Command,
    pub term: Term,
}

pub trait Storage: std::fmt::Debug + Send {
    fn set(&mut self, key: &str, value: Vec<u8>);
    fn get(&self, key: &str) -> Option<&Vec<u8>>;
    fn has_data(&self) -> bool;
}
