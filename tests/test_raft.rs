mod common;
use toy_raft::{bootstrap::tracing_config, message::Command, GenericResult, NodeId};

use std::collections::HashSet;
use tokio::time::{sleep, Duration};

// if some test fail it is because we are not waiting long enough
#[tokio::test(flavor = "multi_thread", worker_threads = 3)]
async fn test_all() -> GenericResult<()> {
    let _guard = tracing_config("test.log");

    test_single_leader().await?;
    test_leader_disconnect().await?;
    test_leader_another_disconnect().await?;
    test_disconnect_all_then_restore().await?;
    test_follower_comes_back().await?;
    //test_disconnect_loop().await?;
    test_commit_one_command().await?;
    test_submit_non_leader_fails().await?;
    test_commit_multiple_commands().await?;
    test_commit_with_disconnect_and_recover().await?;
    test_commit_no_quorum().await?;
    test_commit_leader_disconnects().await?;
    test_crash_follower().await?;
    test_crash_then_restart_follower().await?;
    test_crash_then_restart_leader().await?;
    test_crash_then_restart_all().await?;
    test_replace_multiple_log_entries().await?;
    test_crash_after_submit().await?;

    Ok(())
}

async fn test_single_leader() -> GenericResult<()> {
    let mut harness = common::setup().await;
    let result = harness.check_single_leader().await;
    assert!(result.is_ok());

    harness.shutdown().await
}

async fn test_leader_disconnect() -> GenericResult<()> {
    let mut harness = common::setup().await;
    let (orig_leader_id, orig_term) = harness.check_single_leader().await?;
    harness.disconnect(orig_leader_id).await?;
    sleep(Duration::from_millis(350)).await;

    let (new_leader_id, new_term) = harness.check_single_leader().await?;
    assert_ne!(new_leader_id, orig_leader_id);
    assert!(new_term > orig_term);

    harness.shutdown().await
}

async fn test_leader_another_disconnect() -> GenericResult<()> {
    let mut harness = common::setup().await;
    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    harness.disconnect(orig_leader_id).await?;
    let other_id = (orig_leader_id + 1) % harness.num_nodes() as i32;
    harness.disconnect(other_id).await?;

    // no quorum
    sleep(Duration::from_millis(400)).await;
    let res = harness.check_single_leader().await;
    assert!(res.is_err());

    // reconnect one other server; now we'll have quorim
    harness.reconnect(other_id).await?;
    let res = harness.check_single_leader().await;
    assert!(res.is_ok());

    harness.shutdown().await
}

async fn test_disconnect_all_then_restore() -> GenericResult<()> {
    let mut harness = common::setup().await;

    //	Disconnect all servers from the start. There will be no leader.
    sleep(Duration::from_millis(100)).await;
    for i in 0..harness.num_nodes() as toy_raft::NodeId {
        harness.disconnect(i).await?;
    }
    sleep(Duration::from_millis(400)).await;
    let res = harness.check_single_leader().await;
    assert!(res.is_err());

    // Reconnect all servers. A leader will be found.
    for i in 0..harness.num_nodes() as toy_raft::NodeId {
        harness.reconnect(i).await?;
    }
    let res = harness.check_single_leader().await;
    assert!(res.is_ok());

    harness.shutdown().await
}

async fn test_follower_comes_back() -> GenericResult<()> {
    let mut harness = common::setup().await;
    let (orig_leader_id, orig_term) = harness.check_single_leader().await?;

    let other_id = (orig_leader_id + 1) % harness.num_nodes() as i32;
    harness.disconnect(other_id).await?;
    sleep(Duration::from_millis(400)).await;
    harness.reconnect(other_id).await?;
    sleep(Duration::from_millis(150)).await;

    // We can't have an assertion on the new leader id here because it depends
    // on the relative election timeouts. We can assert that the term changed,
    // however, which implies that re-election has occurred.
    let (_new_leader_id, new_term) = harness.check_single_leader().await?;
    assert!(new_term > orig_term);

    harness.shutdown().await
}

// async fn test_disconnect_loop() -> GenericResult<()> {
//     let mut harness = common::setup().await;

//     for _cycle in 0..3 {
//         let (leader_id, _term) = harness.check_single_leader().await?;
//         harness.disconnect(leader_id).await?;
//         let other_id = (leader_id + 1) % harness.num_nodes() as i32;
//         harness.disconnect(other_id).await?;
//         sleep(Duration::from_millis(300)).await;
//         let res = harness.check_single_leader().await;
//         assert!(res.is_err());

//         harness.reconnect(other_id).await?;
//         harness.reconnect(leader_id).await?;

//         sleep(Duration::from_millis(150)).await;
//     }

//     harness.shutdown().await
// }

async fn test_commit_one_command() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let is_leader = harness.submit_command(orig_leader_id, Command::Get).await?;
    assert!(is_leader);
    sleep(Duration::from_millis(500)).await;

    for i in 0..harness.num_nodes() {
        let received_cmds = harness.client_commands(i as NodeId);
        assert_eq!(1, received_cmds.len());
        assert_eq!(Command::Get, received_cmds[0]);
    }

    harness.shutdown().await
}

async fn test_submit_non_leader_fails() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let sid = (orig_leader_id + 1) % harness.num_nodes() as NodeId;
    let is_leader = harness.submit_command(sid, Command::Get).await?;
    assert!(!is_leader);

    harness.shutdown().await
}

async fn test_commit_multiple_commands() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = HashSet::from([Command::Get, Command::Add(1), Command::Delete]);
    for cmd in cmds.iter() {
        let is_leader = harness.submit_command(orig_leader_id, cmd.clone()).await?;
        assert!(is_leader);
    }
    sleep(Duration::from_millis(350)).await;

    for i in 0..harness.num_nodes() {
        let received_cmds = harness.client_commands(i as NodeId);
        assert_eq!(cmds.len(), received_cmds.len());
        for rcmd in received_cmds.iter() {
            assert!(cmds.contains(rcmd));
        }
    }

    harness.shutdown().await
}

async fn test_commit_with_disconnect_and_recover() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = HashSet::from([Command::Get, Command::Add(1)]);
    for cmd in cmds.iter() {
        let is_leader = harness.submit_command(orig_leader_id, cmd.clone()).await?;
        assert!(is_leader);
    }
    sleep(Duration::from_millis(300)).await;

    for i in 0..harness.num_nodes() {
        let received_cmds = harness.client_commands(i as NodeId);
        assert_eq!(cmds.len(), received_cmds.len());
        for rcmd in received_cmds.iter() {
            assert!(cmds.contains(rcmd));
        }
    }

    let del_id = (orig_leader_id + 1) % harness.num_nodes() as NodeId;
    harness.disconnect(del_id).await?;
    sleep(Duration::from_millis(150)).await;

    // Submit a new command; it will be committed but only to two servers.
    let new_cmd = Command::Delete;
    harness
        .submit_command(orig_leader_id, new_cmd.clone())
        .await?;
    sleep(Duration::from_millis(150)).await;

    let peers_got_new_cmd: Vec<NodeId> = (0..harness.num_nodes() as NodeId)
        .filter(|p| p != &del_id)
        .collect();
    for i in peers_got_new_cmd.into_iter() {
        assert!(harness.client_has_cmd(i, &new_cmd));
    }
    assert!(!harness.client_has_cmd(del_id, &new_cmd));

    // Now reconnect del_id and wait a bit; it should find the new command too
    harness.reconnect(del_id).await?;
    sleep(Duration::from_millis(700)).await;
    assert!(harness.client_has_cmd(del_id, &new_cmd));

    harness.shutdown().await
}

async fn test_commit_no_quorum() -> GenericResult<()> {
    let mut harness = common::setup().await;

    // Submit a couple of values to a fully connected cluster
    let (orig_leader_id, orig_term) = harness.check_single_leader().await?;
    let cmds = HashSet::from([Command::Get, Command::Add(1)]);
    for cmd in cmds.iter() {
        let is_leader = harness.submit_command(orig_leader_id, cmd.clone()).await?;
        assert!(is_leader);
    }
    sleep(Duration::from_millis(400)).await;

    for i in 0..harness.num_nodes() {
        let received_cmds = harness.client_commands(i as NodeId);
        assert_eq!(cmds.len(), received_cmds.len());
        for rcmd in received_cmds.iter() {
            assert!(cmds.contains(rcmd));
        }
    }

    // Disconnect both followers.
    let del_id1 = (orig_leader_id + 1) % harness.num_nodes() as NodeId;
    let del_id2 = (orig_leader_id + 2) % harness.num_nodes() as NodeId;
    harness.disconnect(del_id1).await?;
    harness.disconnect(del_id2).await?;
    sleep(Duration::from_millis(150)).await;

    let new_cmd = Command::Delete;
    harness
        .submit_command(orig_leader_id, new_cmd.clone())
        .await?;
    sleep(Duration::from_millis(150)).await;
    assert!(!harness.client_has_cmd(orig_leader_id, &new_cmd));
    assert!(!harness.client_has_cmd(del_id1, &new_cmd));
    assert!(!harness.client_has_cmd(del_id2, &new_cmd));

    // Reconnect both other servers, we'll have quorum now.
    harness.reconnect(del_id1).await?;
    harness.reconnect(del_id2).await?;
    sleep(Duration::from_millis(600)).await;

    // new_cmd is still not committed because the term has changed.
    assert!(!harness.client_has_cmd(orig_leader_id, &new_cmd));
    assert!(!harness.client_has_cmd(del_id1, &new_cmd));
    assert!(!harness.client_has_cmd(del_id2, &new_cmd));

    // A new leader will be elected. It could be a different leader, even
    // though the original's log is longer, because the two reconnected peers
    // can elect each other
    let (new_leader_id, again_term) = harness.check_single_leader().await?;
    assert_ne!(orig_term, again_term);

    // But new values will be committed for sure...
    let newest_cmds = HashSet::from([Command::Add(9), Command::Add(10)]);
    for cmd in newest_cmds.iter() {
        let is_leader = harness.submit_command(new_leader_id, cmd.clone()).await?;
        assert!(is_leader);
    }
    sleep(Duration::from_millis(300)).await;

    for i in 0..harness.num_nodes() {
        for cmd in newest_cmds.iter() {
            assert!(harness.client_has_cmd(i as NodeId, cmd));
        }
    }

    harness.shutdown().await
}

async fn test_commit_leader_disconnects() -> GenericResult<()> {
    let mut harness = common::setup().await;

    // Submit a couple of values to a fully connected cluster
    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = HashSet::from([Command::Add(5), Command::Add(6)]);
    for cmd in cmds.iter() {
        let is_leader = harness.submit_command(orig_leader_id, cmd.clone()).await?;
        assert!(is_leader);
    }
    sleep(Duration::from_millis(300)).await;

    for i in 0..harness.num_nodes() {
        let received_cmds = harness.client_commands(i as NodeId);
        assert_eq!(cmds.len(), received_cmds.len());
        for rcmd in received_cmds.iter() {
            assert!(cmds.contains(rcmd));
        }
    }

    // Leader disconnected...
    harness.disconnect(orig_leader_id).await?;
    sleep(Duration::from_millis(10)).await;
    // Submit 7 to original leader, even though it's disconnected.
    let seven = Command::Add(7);
    harness
        .submit_command(orig_leader_id, seven.clone())
        .await?;

    sleep(Duration::from_millis(150)).await;
    for i in 0..harness.num_nodes() {
        assert!(!harness.client_has_cmd(i as NodeId, &seven));
    }

    let (new_leader_id, _new_term) = harness.check_single_leader().await?;

    // Submit 8 to new leader.
    let eight = Command::Add(8);
    harness.submit_command(new_leader_id, eight.clone()).await?;
    sleep(Duration::from_millis(150)).await;

    let peers_got_eight: Vec<NodeId> = (0..harness.num_nodes() as NodeId)
        .filter(|p| p != &orig_leader_id)
        .collect();
    for i in peers_got_eight.into_iter() {
        assert!(harness.client_has_cmd(i, &eight));
    }
    assert!(!harness.client_has_cmd(orig_leader_id, &eight));

    // Reconnect old leader and let it settle. The old leader shouldn't be
    // the one winning
    harness.reconnect(orig_leader_id).await?;
    sleep(Duration::from_millis(600)).await;

    let (final_leader_id, _final_term) = harness.check_single_leader().await?;
    assert_ne!(final_leader_id, orig_leader_id);

    // Submit 9 and check it's fully committed.
    let nine = Command::Add(9);
    harness
        .submit_command(final_leader_id, nine.clone())
        .await?;
    sleep(Duration::from_millis(150)).await;

    for i in 0..harness.num_nodes() as NodeId {
        assert!(harness.client_has_cmd(i, &eight));
        assert!(harness.client_has_cmd(i, &nine));
    }
    // But 7 is not committed...
    for i in 0..harness.num_nodes() {
        assert!(!harness.client_has_cmd(i as NodeId, &seven));
    }

    harness.shutdown().await
}

async fn test_crash_follower() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;

    let five = Command::Add(5);
    harness.submit_command(orig_leader_id, five.clone()).await?;
    sleep(Duration::from_millis(350)).await;
    for i in 0..harness.num_nodes() {
        assert!(harness.client_has_cmd(i as NodeId, &five));
    }

    let crash_id = (orig_leader_id + 1) % harness.num_nodes() as NodeId;
    harness.crash_peer(crash_id).await?;
    sleep(Duration::from_millis(350)).await;

    let non_crashed: Vec<NodeId> = (0..harness.num_nodes() as NodeId)
        .filter(|p| p != &crash_id)
        .collect();
    for i in non_crashed.into_iter() {
        assert!(harness.client_has_cmd(i, &five));
    }
    assert!(!harness.client_has_cmd(crash_id, &five));

    harness.shutdown().await
}

async fn test_crash_then_restart_follower() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = [Command::Add(5), Command::Add(6), Command::Add(7)];
    for cmd in cmds.iter() {
        harness.submit_command(orig_leader_id, cmd.clone()).await?;
    }
    sleep(Duration::from_millis(350)).await;

    for i in 0..harness.num_nodes() {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i as NodeId, cmd));
        }
    }

    let crash_id = (orig_leader_id + 1) % harness.num_nodes() as NodeId;
    harness.crash_peer(crash_id).await?;
    sleep(Duration::from_millis(350)).await;

    let non_crashed: Vec<NodeId> = (0..harness.num_nodes() as NodeId)
        .filter(|p| p != &crash_id)
        .collect();
    for cmd in cmds.iter() {
        for i in non_crashed.iter() {
            assert!(harness.client_has_cmd(*i, cmd));
        }
        assert!(!harness.client_has_cmd(crash_id, cmd));
    }

    // Restart the crashed follower and give it some time to come up-to-date
    harness.restart_peer(crash_id).await?;
    sleep(Duration::from_millis(550)).await;
    for i in 0..harness.num_nodes() {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i as NodeId, cmd));
        }
    }

    harness.shutdown().await
}

async fn test_crash_then_restart_leader() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = [Command::Add(5), Command::Add(6), Command::Add(7)];
    for cmd in cmds.iter() {
        harness.submit_command(orig_leader_id, cmd.clone()).await?;
    }
    sleep(Duration::from_millis(350)).await;

    harness.crash_peer(orig_leader_id).await?;
    sleep(Duration::from_millis(350)).await;

    let non_crashed: Vec<NodeId> = (0..harness.num_nodes() as NodeId)
        .filter(|p| p != &orig_leader_id)
        .collect();
    for cmd in cmds.iter() {
        for i in non_crashed.iter() {
            assert!(harness.client_has_cmd(*i, cmd));
        }
        assert!(!harness.client_has_cmd(orig_leader_id, cmd));
    }

    harness.restart_peer(orig_leader_id).await?;
    sleep(Duration::from_millis(550)).await;
    for i in 0..harness.num_nodes() {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i as NodeId, cmd));
        }
    }

    harness.shutdown().await
}

async fn test_crash_then_restart_all() -> GenericResult<()> {
    let mut harness = common::setup().await;

    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = [Command::Add(5), Command::Add(6), Command::Add(7)];
    for cmd in cmds.iter() {
        harness.submit_command(orig_leader_id, cmd.clone()).await?;
    }
    sleep(Duration::from_millis(350)).await;
    let num_nodes = harness.num_nodes() as NodeId;
    for i in 0..num_nodes {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i, cmd));
        }
    }

    for i in 0..num_nodes {
        harness.crash_peer((orig_leader_id + i) % num_nodes).await?;
    }
    sleep(Duration::from_millis(350)).await;
    for i in 0..num_nodes {
        harness
            .restart_peer((orig_leader_id + i) % num_nodes)
            .await?;
    }
    sleep(Duration::from_millis(350)).await;

    let (new_leader_id, _new_term) = harness.check_single_leader().await?;
    harness
        .submit_command(new_leader_id, Command::Add(8))
        .await?;
    sleep(Duration::from_millis(350)).await;

    let cmds = [
        Command::Add(5),
        Command::Add(6),
        Command::Add(7),
        Command::Add(8),
    ];
    for i in 0..num_nodes {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i, cmd));
        }
    }

    harness.shutdown().await
}

async fn test_replace_multiple_log_entries() -> GenericResult<()> {
    let mut harness = common::setup().await;

    // Submit a couple of values to a fully connected cluster.
    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    let cmds = [Command::Add(5), Command::Add(6)];
    for cmd in cmds.iter() {
        harness.submit_command(orig_leader_id, cmd.clone()).await?;
    }
    sleep(Duration::from_millis(350)).await;
    for i in 0..harness.num_nodes() {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i as NodeId, cmd));
        }
    }
    // Leader disconnected...
    harness.crash_peer(orig_leader_id).await?;
    sleep(Duration::from_millis(350)).await;

    // Submit a few entries to the original leader; it's disconnected, so they
    // won't be replicated.
    // TODO: we can not send commands to disconnected node
    //
    // let cmds = [
    //     Command::Add(21),
    //     Command::Add(22),
    //     Command::Add(23),
    //     Command::Add(24),
    // ];
    // for cmd in cmds.iter() {
    //     harness.submit_command(orig_leader_id, cmd.clone()).await?;
    // }
    // sleep(Duration::from_millis(250)).await;

    let (new_leader_id, _new_term) = harness
        .check_single_leader_filtered(&[orig_leader_id])
        .await?;
    sleep(Duration::from_millis(350)).await;
    // Submit entries to new leader -- these will be replicated.
    let cmds = [Command::Add(8), Command::Add(9), Command::Add(10)];
    for cmd in cmds.iter() {
        harness.submit_command(new_leader_id, cmd.clone()).await?;
    }
    sleep(Duration::from_millis(350)).await;

    let non_crashed: Vec<NodeId> = (0..harness.num_nodes() as NodeId)
        .filter(|p| p != &orig_leader_id)
        .collect();
    for i in non_crashed.iter() {
        assert!(harness.client_has_cmd(*i, &Command::Add(10)));
    }
    for i in 0..harness.num_nodes() as NodeId {
        assert!(!harness.client_has_cmd(i, &Command::Add(21)));
    }

    // Crash/restart new leader to reset its nextIndex, to ensure that the
    // new leader of the cluster (could be the third server after elections)
    // tries to replace the original's servers unreplicated entries from
    // the very end.
    harness.crash_peer(new_leader_id).await?;
    sleep(Duration::from_millis(350)).await;
    harness.restart_peer(new_leader_id).await?;
    sleep(Duration::from_millis(350)).await;

    let (final_leader_id, _final_term) = harness
        .check_single_leader_filtered(&[orig_leader_id])
        .await?;
    harness.restart_peer(orig_leader_id).await?;
    sleep(Duration::from_millis(350)).await;

    // Submit another entry; this is because leaders won't commit entries from
    // previous terms so the 8,9,10 may not be committed everywhere
    // after the restart before a new command comes it
    harness
        .submit_command(final_leader_id, Command::Add(11))
        .await?;
    sleep(Duration::from_millis(350)).await;

    // At this point, 11 and 10 should be replicated everywhere; 21 won't be.
    let commited_cmds = [Command::Add(11), Command::Add(10)];
    let uncommited_cmds = [Command::Add(21)];
    for i in 0..harness.num_nodes() {
        for cmd in commited_cmds.iter() {
            assert!(harness.client_has_cmd(i as NodeId, cmd));
        }
    }
    for i in 0..harness.num_nodes() {
        for cmd in uncommited_cmds.iter() {
            assert!(!harness.client_has_cmd(i as NodeId, cmd));
        }
    }

    harness.shutdown().await
}

async fn test_crash_after_submit() -> GenericResult<()> {
    let mut harness = common::setup().await;

    // Wait for a leader to emerge, and submit a command - then immediately
    // crash; the leader should have no time to send an updated LeaderCommit
    // to followers. It doesn't have time to get back AE responses either, so
    // the leader itself won't send it on the commit channel.
    let (orig_leader_id, _orig_term) = harness.check_single_leader().await?;
    harness
        .submit_command(orig_leader_id, Command::Add(5))
        .await?;
    //sleep(Duration::from_millis(1)).await;
    harness.crash_peer(orig_leader_id).await?;

    // Make sure 5 is not committed when a new leader is elected.
    // Leaders won't commit commands from previous terms.
    //sleep(Duration::from_millis(350)).await;
    //let (new_leader_id, _new_term) = harness.check_single_leader().await?;
    sleep(Duration::from_millis(350)).await;
    for i in 0..harness.num_nodes() as NodeId {
        assert!(!harness.client_has_cmd(i, &Command::Add(5)));
    }

    // The old leader restarts. After a while, 5 is still not committed.
    harness.restart_peer(orig_leader_id).await?;
    sleep(Duration::from_millis(550)).await;
    let (new_leader_id, _new_term) = harness.check_single_leader().await?;
    sleep(Duration::from_millis(350)).await;
    for i in 0..harness.num_nodes() as NodeId {
        assert!(!harness.client_has_cmd(i, &Command::Add(5)));
    }

    // When we submit a new command, it will be submitted, and so will 5,
    // because it appears in everyone's logs
    harness
        .submit_command(new_leader_id, Command::Add(6))
        .await?;
    sleep(Duration::from_millis(350)).await;
    let cmds = [Command::Add(5), Command::Add(6)];
    for i in 0..harness.num_nodes() as NodeId {
        for cmd in cmds.iter() {
            assert!(harness.client_has_cmd(i, cmd));
        }
    }

    harness.shutdown().await
}
