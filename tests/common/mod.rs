use toy_raft::{
    bootstrap::{read_config_file, NodeCfg, ServerTestingChannels},
    client::{start_receiving_commit_ch, Client},
    message::{Command, CommitEntry, Storage},
    raft::CmState,
    server::{Server, NODES},
    GenericResult, NodeId, Term,
};

use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use tokio::sync::mpsc;
use tokio::task::{JoinHandle, JoinSet};

const CH_SIZE: usize = 32;

pub async fn setup() -> TestHarness {
    let nodes = NODES.get_or_init(|| read_config_file("config.ini"));
    start_testing(nodes.to_vec()).await
}

pub struct BootstrapTestingChannels {
    play_dead_txs: HashMap<NodeId, tokio::sync::mpsc::Sender<bool>>,
    terminate_txs: HashMap<NodeId, tokio::sync::mpsc::Sender<()>>,
    report_req_txs: HashMap<NodeId, tokio::sync::mpsc::Sender<()>>,
    report_res_rx: tokio::sync::mpsc::Receiver<(NodeId, Term, CmState)>,
    report_res_tx: tokio::sync::mpsc::Sender<(NodeId, Term, CmState)>,
    submit_req_txs: HashMap<NodeId, tokio::sync::mpsc::Sender<Command>>,
    submit_res_rx: tokio::sync::mpsc::Receiver<bool>,
    submit_res_tx: tokio::sync::mpsc::Sender<bool>,
}

pub struct TestHarness {
    servers_join_set: JoinSet<()>,
    test_channels: BootstrapTestingChannels,
    clients: HashMap<NodeId, Client>,
    storages: HashMap<NodeId, Arc<Mutex<dyn Storage>>>,
    commit_ch_handles: HashMap<NodeId, JoinHandle<()>>,
    num_nodes: u16,
    alive: HashMap<NodeId, bool>,
}

impl TestHarness {
    pub async fn check_single_leader_filtered(
        &mut self,
        ids: &[NodeId],
    ) -> GenericResult<(NodeId, Term)> {
        let active_nodes = (0..self.num_nodes as NodeId)
            .filter(|n| !ids.contains(n))
            .collect();
        self.single_leader(active_nodes).await
    }

    pub async fn check_single_leader(&mut self) -> GenericResult<(NodeId, Term)> {
        let active_nodes = (0..self.num_nodes as NodeId).collect();
        self.single_leader(active_nodes).await
    }

    // CheckSingleLeader checks that only a single server thinks it's the
    // leader. Returns the leader's id and term. It retries several times
    // if no leader is identified yet
    async fn single_leader(&mut self, active_nodes: Vec<NodeId>) -> GenericResult<(NodeId, Term)> {
        let num_retries = 5;
        for _ in 0..num_retries {
            let mut leader_id = -1;
            let mut leader_term = -1;
            for i in active_nodes.iter() {
                let (id, term, state) = self.node_report(*i).await?;
                if state == CmState::Leader {
                    if leader_id < 0 {
                        leader_id = id;
                        leader_term = term;
                    } else {
                        return Err(
                            format!("both {leader_id} and {id} think they are leaders").into()
                        );
                    }
                }
            }
            if leader_id >= 0 {
                return Ok((leader_id, leader_term));
            }
            tokio::time::sleep(tokio::time::Duration::from_millis(150)).await;
        }

        Err("leader not found".into())
    }

    pub async fn disconnect(&self, id: NodeId) -> GenericResult<()> {
        self.test_channels.play_dead_txs[&id]
            .send(true)
            .await
            .map_err(|e| e.into())
    }

    pub async fn reconnect(&self, id: NodeId) -> GenericResult<()> {
        self.test_channels.play_dead_txs[&id]
            .send(false)
            .await
            .map_err(|e| e.into())
    }

    pub async fn crash_peer(&mut self, id: NodeId) -> GenericResult<()> {
        self.clients[&id].clear_commands();
        self.alive.insert(id, false);
        self.test_channels.terminate_txs[&id]
            .send(())
            .await
            .map_err(|e| e.into())
    }

    // TODO: not cool
    pub async fn restart_peer(&mut self, id: NodeId) -> GenericResult<()> {
        if let Some(true) = self.alive.get(&id) {
            tracing::error!("Node {id} is alive. Can not restart it");
            return Ok(());
        }
        let (play_dead_tx, play_dead_rx) = mpsc::channel(CH_SIZE);
        self.test_channels.play_dead_txs.insert(id, play_dead_tx);
        let (terminate_tx, terminate_rx) = mpsc::channel(CH_SIZE);
        self.test_channels.terminate_txs.insert(id, terminate_tx);
        let (report_req_tx, report_req_rx) = mpsc::channel(CH_SIZE);
        self.test_channels.report_req_txs.insert(id, report_req_tx);
        let (submit_req_tx, submit_req_rx) = mpsc::channel(CH_SIZE);
        self.test_channels.submit_req_txs.insert(id, submit_req_tx);

        let server_tch = ServerTestingChannels {
            play_dead_rx,
            terminate_rx,
            report_req_rx,
            submit_req_rx,
            report_res_tx: self.test_channels.report_res_tx.clone(),
            submit_res_tx: self.test_channels.submit_res_tx.clone(),
        };

        let received_cmds = self.clients[&id].received_commands.clone();
        let (commit_tx, commit_rx) = mpsc::unbounded_channel::<CommitEntry>();
        let join_handle =
            start_receiving_commit_ch(commit_rx, received_cmds, self.clients[&id].id).await;
        self.commit_ch_handles.insert(id, join_handle);

        let node_cfg = NODES.get().unwrap().iter().find(|&n| n.id == id).unwrap();
        let peers = node_cfg.peers.clone();
        let listen = node_cfg.listen.clone();
        let storage = self.storages[&id].clone();
        let server = Server::new(id, peers, storage);
        self.servers_join_set
            .spawn(server.serve(listen, server_tch, commit_tx, true));
        Ok(())
    }

    pub async fn submit_command(&mut self, id: NodeId, cmd: Command) -> GenericResult<bool> {
        self.test_channels.submit_req_txs[&id].send(cmd).await?;
        self.test_channels
            .submit_res_rx
            .recv()
            .await
            .ok_or("submit_res_tx is closed".into())
    }

    pub async fn node_report(&mut self, id: NodeId) -> GenericResult<(NodeId, Term, CmState)> {
        self.test_channels.report_req_txs[&id].send(()).await?;
        self.test_channels
            .report_res_rx
            .recv()
            .await
            .ok_or("report_res_tx is closed".into())
    }

    pub fn client_commands(&self, id: NodeId) -> Vec<Command> {
        self.clients[&id].commands()
    }

    pub fn client_has_cmd(&self, id: NodeId, cmd: &Command) -> bool {
        self.clients[&id].has_command(cmd)
    }

    pub async fn shutdown(&mut self) -> GenericResult<()> {
        self.servers_join_set.shutdown().await;
        for handle in self.commit_ch_handles.values() {
            handle.abort();
        }
        let node_ids: Vec<NodeId> = self.commit_ch_handles.keys().cloned().collect();
        for node_id in node_ids.iter() {
            let handle = self.commit_ch_handles.remove(node_id).unwrap();
            handle.await?;
        }
        Ok(())
    }

    pub fn num_nodes(&self) -> u16 {
        self.num_nodes
    }
}

#[derive(Debug)]
struct MapStorage {
    db: HashMap<String, Vec<u8>>,
}

impl MapStorage {
    fn new() -> Self {
        Self { db: HashMap::new() }
    }
}

impl Storage for MapStorage {
    fn set(&mut self, key: &str, value: Vec<u8>) {
        self.db.insert(key.to_string(), value);
    }

    fn get(&self, key: &str) -> Option<&Vec<u8>> {
        self.db.get(key)
    }

    fn has_data(&self) -> bool {
        !self.db.is_empty()
    }
}

pub async fn start_testing(nodes: Vec<NodeCfg>) -> TestHarness {
    let mut play_dead_txs = HashMap::new();
    let mut terminate_txs = HashMap::new();
    let mut report_req_txs = HashMap::new();
    let mut submit_req_txs = HashMap::new();
    let (report_res_tx, report_res_rx) = mpsc::channel(CH_SIZE);
    let (submit_res_tx, submit_res_rx) = mpsc::channel(CH_SIZE);
    let mut server_tchs = HashMap::new();

    let nodes_len = nodes.len();
    for node in nodes.iter() {
        let (play_dead_tx, play_dead_rx) = mpsc::channel(CH_SIZE);
        play_dead_txs.insert(node.id, play_dead_tx);
        let (terminate_tx, terminate_rx) = mpsc::channel(CH_SIZE);
        terminate_txs.insert(node.id, terminate_tx);
        let (report_req_tx, report_req_rx) = mpsc::channel(CH_SIZE);
        report_req_txs.insert(node.id, report_req_tx);
        let (submit_req_tx, submit_req_rx) = mpsc::channel(CH_SIZE);
        submit_req_txs.insert(node.id, submit_req_tx);

        let server_tch = ServerTestingChannels {
            play_dead_rx,
            terminate_rx,
            report_req_rx,
            submit_req_rx,
            report_res_tx: report_res_tx.clone(),
            submit_res_tx: submit_res_tx.clone(),
        };
        server_tchs.insert(node.id, server_tch);
    }

    let test_channels = BootstrapTestingChannels {
        play_dead_txs,
        terminate_txs,
        report_req_txs,
        submit_req_txs,
        report_res_rx,
        report_res_tx,
        submit_res_rx,
        submit_res_tx,
    };

    let mut join_set = JoinSet::new();
    let mut clients = HashMap::new();
    let mut storages = HashMap::new();
    let mut alive = HashMap::new();
    let mut join_handles = HashMap::new();
    for node in nodes.into_iter() {
        let server_tch = server_tchs.remove(&node.id).unwrap();
        let client = Client {
            id: node.client,
            received_commands: Arc::new(Mutex::new(Vec::new())),
        };
        let storage: Arc<Mutex<dyn Storage>> = Arc::new(Mutex::new(MapStorage::new()));
        let received_cmds = client.received_commands.clone();
        let (commit_tx, commit_rx) = mpsc::unbounded_channel::<CommitEntry>();
        let join_handle = start_receiving_commit_ch(commit_rx, received_cmds, client.id).await;
        join_handles.insert(node.id, join_handle);
        let server = Server::new(node.id, node.peers, storage.clone());
        join_set.spawn(server.serve(node.listen, server_tch, commit_tx, false));
        clients.insert(node.id, client);
        storages.insert(node.id, storage);
        alive.insert(node.id, true);
    }

    TestHarness {
        servers_join_set: join_set,
        test_channels,
        clients,
        storages,
        commit_ch_handles: join_handles,
        num_nodes: nodes_len as u16,
        alive,
    }
}
